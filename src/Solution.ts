import {ISolution} from "./ISolution";

class Solution implements ISolution{
    solve(str: string): boolean{
        let holder: any = [];
        let validate: any = { ')': '('};
        let strArr: any = str.split('');
        strArr.forEach((char: string) => {
            if (!validate[char]){
                holder.push(char);
                return
            }
            holder.pop();
        })
        return holder.length === 0;
    }
}
export {Solution}