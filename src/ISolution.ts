export interface ISolution{
    solve(str: string): boolean
}