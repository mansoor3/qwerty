import {ISolution} from "./ISolution";
import {Solution} from "./Solution";

function main(){
    let _case: any = [
        "()",
        ")(",
        "()()",
        "((()))",
        "((())",
        ""
    ];
    let solution = new Solution();
    runCases(_case,solution)
}

function runCases(_case: any, solution: ISolution){
    _case.forEach((str: string) => {
        let result = solution.solve(str);
        console.log(str, "=> ", result)
    });
}

main();